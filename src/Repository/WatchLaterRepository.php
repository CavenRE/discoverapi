<?php

namespace App\Repository;

use App\Entity\WatchLater;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method WatchLater|null find($id, $lockMode = null, $lockVersion = null)
 * @method WatchLater|null findOneBy(array $criteria, array $orderBy = null)
 * @method WatchLater[]    findAll()
 * @method WatchLater[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WatchLaterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WatchLater::class);
    }

    // /**
    //  * @return WatchLater[] Returns an array of WatchLater objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WatchLater
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
