# DiscoverAPI

Syfomy 5 REST API with ReatJS interface and Doctrine ORM

## Installation

Use the package manager [composer](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-macos) to install Symfony5 and Packages.

```bash
composer install
```

Use [yarn](https://classic.yarnpkg.com/en/docs/install) to install Encore packages and ReactJS.

```bash
yarn install
```

> If you dont have a server setup to run the project you can install [symfony](https://symfony.com/download) 
> locally on your machine to run **symfony server:start** which serves the site 
> to **http://127.0.0.1:8000/**

## Usage

Once you are done with the base installation you can then run the required **doctrine** commands
after setting up your **.env** file.

```dotenv
DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7
```

Now that your **.env** variables are setup we can now make the database.

```bash
php bin/console doctrine:database:create
```

And lastly, we can migrate the tables to the database

```bash
php bin/console doctrine:migrations:migrate
```


> Will update Readme.MD as the project progresses

